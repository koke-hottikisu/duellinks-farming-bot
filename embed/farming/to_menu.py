import pyautogui as pa

from . import pyautoguiwrapper as paw


def to_menu():
    print('Back to menu...', end='')

    home_images = ['menu_cart_inactive',
                   'menu_colosseum_inactive',
                   'menu_deck_inactive']
    while not paw.exists_image(home_images):
        paw.click_image(['decide', 'ok', 'next',
                         'cancel', 'back',
                         'home', 'comment'])

    while not paw.exists_image(['menu_gate_active']):
        paw.scroll(1)

    print("done")
