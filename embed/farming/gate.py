from time import sleep

from .to_menu import to_menu
from . import pyautoguiwrapper as paw

def gate():
    """
    ゲートでデュエルしてホーム画面に戻る
    """
    to_menu()

    while not paw.exists_image(['menu_gate_active']):
        sleep(1)
    while paw.exists_image(['menu_gate_active']):
        paw.click_image(['menu_gate_active'])
        sleep(1)

    while not paw.exists_image(['lv10']):
        sleep(1)
    paw.click_image(['lv10'])

    while not paw.exists_image(['duel']):
        sleep(1)
    while paw.exists_image(['duel']):
        paw.click_image(['duel'])
        sleep(1)

    while not paw.exists_image(['comment']):
        sleep(1)
    while paw.exists_image(['comment']):
        paw.click_image(['comment'])
        sleep(1)

    while not paw.exists_image(['duel']):
        sleep(1)
