import os
from glob import glob


IMG_DIR = os.path.dirname(os.path.abspath(__file__))


def image_paths(image_name):
    """
    対応する画像のパスを返す
    """
    return list(glob(IMG_DIR + '/' + image_name + '__*'))


__all__ = [image_paths]
