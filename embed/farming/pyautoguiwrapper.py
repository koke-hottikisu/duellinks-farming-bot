from time import sleep

import pyautogui as pa

from . import image


def get():
    """
    ゲームフレームの座標とサイズを返す
    """
    for path in image.image_paths('gameicon'):
        bounds = pa.locateOnScreen(path)
        if bounds is not None:
            x, y, _, _ = bounds
            return (x, y, 800, 30 + 450)
    raise pa.ImageNotFoundException()


GAMEFRAME_BOUNDS = get()


def locate_on_gameframe_from_path(path, **kwargs):
    """
    画像を検出し座標とサイズを返す
    """
    bounds = pa.locateOnScreen(path, region=GAMEFRAME_BOUNDS, confidence=0.95, **kwargs)
    if bounds is None:
        raise pa.ImageNotFoundException()
    return bounds


def locate_on_gameframe_from_paths(paths, **kwargs):
    """
    画像を検出し座標とサイズを返す
    """
    for path in paths:
        try:
            bounds = locate_on_gameframe_from_path(path, **kwargs)
            return bounds
        except pa.ImageNotFoundException:
            pass
    raise pa.ImageNotFoundException()


def locate_on_gameframe(names, **kwargs):
    for name in names:
        try:
            bounds = locate_on_gameframe_from_paths(image.image_paths(name), **kwargs)
            return bounds, name
        except pa.ImageNotFoundException:
            pass
    raise pa.ImageNotFoundException()


def exists_image(names):
    """
    画像が画面上にあるかどうかを返す
    """
    try:
        locate_on_gameframe(names)
    except pa.ImageNotFoundException:
        return False
    return True


def click_image(names):
    """
    画像をクリックする
    """
    try:
        bounds, _ = locate_on_gameframe(names)
    except pa.ImageNotFoundException:
        return False
    x, y, _, _ = bounds
    pa.click(x, y)
    return True


def active_gameframe():
    pa.click(GAMEFRAME_BOUNDS[0] + 10, GAMEFRAME_BOUNDS[1] + 10)


def scroll(clicks, **xargs):
    pa.click(GAMEFRAME_BOUNDS[0], GAMEFRAME_BOUNDS[1])
    pa.moveTo(GAMEFRAME_BOUNDS[0] + GAMEFRAME_BOUNDS[2] / 2, GAMEFRAME_BOUNDS[1] + GAMEFRAME_BOUNDS[3] / 2)
    sleep(0.5)
    pa.scroll(clicks, **xargs)
    sleep(0.5)
